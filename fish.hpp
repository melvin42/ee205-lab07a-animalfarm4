///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file fish.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Melvin Alhambra <melvin42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31/03/2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "animal.hpp"

namespace animalfarm {

class Fish : public Animal {
public:
   enum Color scaleColor;
	float      favoriteTemp;
   virtual const string speak();
	void printInfo();
};

} // namespace animalfarm


///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalfactory.hpp
/// @version 1.0
///
/// Generates a random animal
///
/// @author Melvin Alhambra <melvin42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31/03/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

using namespace std;

namespace animalfarm {

class AnimalFactory {
public: 
    static Animal* getRandomAnimal(); 

};

} //namespace animalfarm


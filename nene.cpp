///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author @Melvin Alhambra <@melvin42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31/03/2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene(string newTagID, enum Color newColor, enum Gender newGender ) {
   species = "Branta sandvicensis";
   gender = newGender;
   featherColor = newColor;
   tagID=newTagID;
   isMigratory=true;
   }
const string Nene::speak(){
   return string("Nay, nay");
}
void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << tagID << "]" << endl;
   Bird::printInfo();
};
} // namespace animalfarm


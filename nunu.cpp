///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Melvin Alhambra <melvin42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31/03/2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {
	
Nunu::Nunu(bool nativeStatus, enum Color newColor, enum Gender newGender ) {
	species = "Fistularia chinesnsis"; 
   gender = newGender;         
	scaleColor = newColor;       
	favoriteTemp = 80.6;     
   isNative=nativeStatus;
}

void Nunu::printInfo() {
	cout << "Nunu" << endl;
   if (isNative==1)
      cout << "   Is native = [" << "true"  << "]" << endl;
   else
      cout << "   Is native = [" << "false" << "]" << endl;
	Fish::printInfo();
}

} // namespace animalfarm


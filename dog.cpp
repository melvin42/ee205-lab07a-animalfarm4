///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file dog.cpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author @Melvin Alhambra <@melvin42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   31/03/2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "dog.hpp"

using namespace std;

namespace animalfarm {
	
Dog::Dog( string newName, enum Color newColor, enum Gender newGender ) {
	gender = newGender;         
	species = "Canis lupus";    
	hairColor = newColor;       
	gestationPeriod = 64;       
	name = newName;             
}


const string Dog::speak() {
	return string( "Woof" );
}


/// Print our Dog and name first... then print whatever information Mammal holds.
void Dog::printInfo() {
	cout << "Dog Name = [" << name << "]" << endl;
	Mammal::printInfo();
}

} // namespace animalfarm


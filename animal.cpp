///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Melvin Alhambra <melvin42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31/03/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include "animal.hpp"

using namespace std;

namespace animalfarm {

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}

const Gender Animal::getRandomGender(){
   int randomGender = rand()%2;

   if (randomGender==0)
      return MALE;
   else 
      return FEMALE;
   
}

string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};	

const Color Animal::getRandomColor(){
   int color = rand()%6;
      switch(color){
         case 0:    return BLACK;  break;
         case 1:    return WHITE;  break;
         case 2:      return RED;    break;
         case 3:   return SILVER; break;
         case 4:   return YELLOW; break;
         case 5:    return BLACK;  break;
   }
   return BLACK;
}

string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK:    return string("Black");  break;
      case WHITE:    return string("White");  break;
      case RED:      return string("Red");    break;
      case SILVER:   return string("Silver"); break;
      case YELLOW:   return string("Yellow"); break;
      case BROWN:    return string("Brown");  break;
   }

   return string("Unknown");
};

const float Animal::getRandomWeight(const float from, const float to){
   int range = int(to-from);
   float randomWeight=from+ rand()%range;

   return randomWeight;
}

const string Animal::getRandomName(){
   int length =( rand()%6+4);
   int i;
   char randomName[length];
   randomName[0] = (char) (65+rand()%26);
      for (i=1;i<length;i++){
         randomName[i] = (char)(97+rand()%26);
      }
   return randomName;
}

const bool Animal::getRandomBool(){
   int randomBool=rand()%2;
   if (randomBool==0)
      return false;
   else 
      return true;
}
Animal::Animal(){
   cout << "." ;
}

Animal::~Animal(){
   cout << "x" ;
}
}// namespace animalfarm

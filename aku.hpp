///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.hpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author @Melvin Alhambra<@melvin42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31/03/2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>
#include "fish.hpp"

using namespace std;


namespace animalfarm {

class Aku : public Fish {
public:
   float weight;
   Aku(float currentWeight, enum Color newColor, enum Gender newGender );


   void printInfo();
};

} // namespace animalfarm



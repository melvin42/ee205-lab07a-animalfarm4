///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.hpp
/// @version 1.0
///
/// Exports data about all palila
///
/// @author @Melvin Alhambra <@melvin42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31/03/2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>
#include "bird.hpp"

using namespace std;


namespace animalfarm {

class Palila : public Bird {
public:
   string whereFound;
   Palila(string placeStatus, enum Color newColor, enum Gender newGender );


   void printInfo();
};

} // namespace animalfarm

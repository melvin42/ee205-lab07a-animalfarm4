///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author @Melvin Alhambra <@melvin42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
//  @date   31/03/2021
///////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila(string placeStatus, enum Color newColor, enum Gender newGender ) {
   species = "Loxioides baileui";
   gender = newGender;
   featherColor = newColor;
   whereFound=placeStatus;
   isMigratory=false;
   }

void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where Found = [" << whereFound << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm



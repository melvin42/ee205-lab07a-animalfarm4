///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author @Melvin Alhambra <@melvin42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   31/04/2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku(float currentWeight, enum Color newColor, enum Gender newGender ) {
   species = "Katsuwonus pelamis";
   gender = newGender;
   scaleColor = newColor;
   favoriteTemp = 75;
   weight= currentWeight;
}

void Aku::printInfo() {
   cout << "Aku" << endl;
   cout << "   Weight = [" << weight << "]" << endl;
   Fish::printInfo();
}

}






